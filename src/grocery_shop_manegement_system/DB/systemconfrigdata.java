/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grocery_shop_manegement_system.DB;

/**
 *
 * @author isuru
 */
public class systemconfrigdata {
    private static String Activeuser;
    private static String usertype;
    private static String employeeid;

    public static String getActiveuser() {
        return Activeuser;
    }

    public static void setActiveuser(String aActiveuser) {
        Activeuser = aActiveuser;
    }

    public static String getUsertype() {
        return usertype;
    }

    public static void setUsertype(String aUsertype) {
        usertype = aUsertype;
    }

    public static String getEmployeeid() {
        return employeeid;
    }

    public static void setEmployeeid(String aEmployeeid) {
        employeeid = aEmployeeid;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grocery_shop_manegement_system.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author isuru
 */
public class dbclass {
    private static Connection connection;
    
    private static void int_db() throws ClassNotFoundException, SQLException  {
        Class.forName("com.mysql.jdbc.Driver");
        connection =  DriverManager.getConnection("jdbc:mysql://127.0.0.1:3309/inventory", "root", "achi19980815");
    }
    public static void push(String sql) throws Exception{
        if(connection == null){
            int_db();
        }
        connection.createStatement().executeUpdate(sql);
    }
    public static ResultSet search(String sql) throws Exception {
        if(connection == null){
            int_db();
        }
        return connection.createStatement().executeQuery(sql);
    }
     public static Connection getNewConnection()throws Exception{
        if(connection ==null){
            int_db();
        }
        return connection;
    }
}
